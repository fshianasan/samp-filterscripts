// ==FilterScript==
// @name		Teleport Command v1.1
// @author		fshianasan <fshianasan@gmail.com>
// @description 自作テレポコマンドです。(date:2015/07/09)
// @include		<a_samp>
// ==/FilterScript==

#include <a_samp>

#define MAX_TELEPORTS 25

enum E_TELEPORT_INFO {
	Command[64],
	Name[128],
	Float:X,
	Float:Y,
	Float:Z,
	World,
	Interior
};

new gTeleportsInfo[MAX_TELEPORTS][E_TELEPORT_INFO] = {
	{		"/ls",		   "Los Santos",	 2488.8999,   -1671.3000,	 13.3359, 0,	 0},
	{		"/sf",	       "San Fierro",	-2026.7000,		143.2000,	 28.8359, 0,	 0},
	{		"/lv",       "Las Ventulas",	 2032.1000,		997.0000,	 10.8131, 0,	 0},
 	{	"/lvhill", 		  "The Big Ear", 	 -260.0000,	   1538.5000,	 75.5625, 0,	 0},
 	{	"/sfhill", 				 "SF峠", 	-2506.2000,	   -613.2000,	132.5625, 0,	 0},
 	{	"/lshill",			"LS 観測所",	 1246.2288,	  -2057.7507,	 59.5123, 0,	 0},
 	{	  "/yama", 		   "Chiliad 山", 	-2343.3999,	  -1603.2000,	483.6754, 0,	 0},
 	{	  "/kacc", 			  "K.A.C.C",	 2510.2000,    2773.3000,	 10.8203, 0,	 0},
 	{      "/dam", 		  "Sherman Dam",	 -917.8000,    2017.4000,	 60.9141, 0,	 0},
 	{ 	   "/a51", 			  "Area 51",	  214.4000,	   1907.6000,	 17.6406, 0,	 0},
	{	  "/wang",			"Wang Cars",	-1992.4138,		288.1571,	 33.9107, 0,	 0},
 	{  	  "/otto", 		   "Otto's Car",	-1640.8734,	   1202.8651,	  7.2400, 0,	 0},
 	{	  "/lsap", 		   "LS Airport",	 1686.7000,	  -2450.2000,	 13.5547, 0,	 0},
 	{	  "/sfap", 		   "SF Airport",	-1345.0000,    -229.8000,	 14.1484, 0,	 0},
 	{ 	  "/lvap", 		   "LV Airport",	 1435.5000,    1463.2000,	 10.8203, 0,	 0},
 	{	  "/lspd", 	   "LS Police Dept",	 1544.5394,	  -1627.3228, 	 13.3828, 0,	 0},
 	{	  "/sfpd", 	   "SF Police Dept",	-1345.0000,    -229.8000,	 14.1484, 0,	 0},
 	{ 	  "/lvpd", 	   "LV Police Dept",	 2237.2317,	   2453.4922, 	 10.6782, 0,	 0},
 	{    "/tinko", 					"?",	 -426.9000,    2505.6001,	124.3047, 0, 	 0},
 	{     "/dsaf", 		   "砂漠飛行場", 	  350.7033,	   2539.2002,	 16.3046, 0,	 0},
 	{    	"/lc", 		 "Liberty City", 	   -750.80,		  491.00, 	 1371.70, 0,	 1},
 	{   "/8track", 	  "8-Track Stadium",	 -1395.958,		-208.197, 	1051.170, 0,	 7},
 	{    "/dirtb", 	"Dirt Bike Stadium", 	-1424.9319,	   -664.5869,  1059.8585, 0,	 4},
 	{  	"/bloodb", "Blood Bowl Stadium", 	  -1394.20,	   	  987.62,  	 1023.96, 0,	15},
 	{    "/kicks",  "Kickstart Stadium", 	  -1410.72,	   	 1591.16,  	 1052.53, 0,	14}
}; // {"Command" , X, Y, Z, World, Interior}

// -----------------------------------------------------------------------------

public OnPlayerCommandText(playerid, cmdtext[]) {
	new cmd[256];
	new idx;

	cmd = strtok(cmdtext, idx);

	for(new i=0; i<MAX_TELEPORTS; i++) {
		if (strcmp(gTeleportsInfo[i][Command], cmd, true) == 0) {
			new Float:Angle;
			GetPlayerFacingAngle(playerid, Angle);

			if(IsPlayerInAnyVehicle(playerid)) {
				SetVehiclePos(GetPlayerVehicleID(playerid), gTeleportsInfo[i][X], gTeleportsInfo[i][Y], gTeleportsInfo[i][Z]);
				SetVehicleZAngle(GetPlayerVehicleID(playerid), Angle);
				LinkVehicleToInterior(GetPlayerVehicleID(playerid), gTeleportsInfo[i][Interior]);
				SetPlayerInterior(playerid, gTeleportsInfo[i][Interior]);
				SetPlayerVirtualWorld(playerid, gTeleportsInfo[i][World]);
			} else {
				SetPlayerPos(playerid, gTeleportsInfo[i][X], gTeleportsInfo[i][Y], gTeleportsInfo[i][Z]);
				SetPlayerFacingAngle(playerid, Angle);
				SetPlayerInterior(playerid, gTeleportsInfo[i][Interior]);
				SetPlayerVirtualWorld(playerid, gTeleportsInfo[i][World]);
			}

			new name[MAX_PLAYER_NAME], str[128];
			GetPlayerName(playerid, name, sizeof(name));
			format(str, sizeof(str), "[INFO] %s は %s (%s) に移動しました。", name, gTeleportsInfo[i][Name], gTeleportsInfo[i][Command]);
			SendClientMessageToAll(0x6090EFFF, str);

			printf("[info] /tp %s to %s(%s)", name, gTeleportsInfo[i][Name], gTeleportsInfo[i][Command]);

			return 1;
		}
	}
	return 0;
}

// -----------------------------------------------------------------------------

stock strtok(const string[], &index)
{
	new length = strlen(string);
	while ((index < length) && (string[index] <= ' '))
	{
		index++;
	}

	new offset = index;
	new result[20];
	while ((index < length) && (string[index] > ' ') && ((index - offset) < (sizeof(result) - 1)))
	{
		result[index - offset] = string[index];
		index++;
	}
	result[index - offset] = EOS;
	return result;
}

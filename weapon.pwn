// ==FilterScript==
// @name		weapon_spawner v1.1
// @author		fshianasan <fshianasan@gmail.com>
// @description	/weapon と入力するとメニューが出てスロットを選ぶことによって武器が出せる、という FS。(date:2015/06/09)
// @include		<a_samp>
// ==/FilterScript==

#include <a_samp>

new Menu:WeaponMenu[13];

// -----------------------------------------------------------------------------

public OnPlayerCommandText(playerid, cmdtext[]) {
	new cmd[256];
	new idx;
	cmd = strtok(cmdtext, idx);

	if(strcmp("/weapon", cmd, true) == 0) {
    	ShowMenuForPlayer(WeaponMenu[0], playerid);
		SendClientMessage(playerid, 0xFFFF00FF, "* Weapon Spawner Menu");
    	// TogglePlayerControllable(playerid, 0);
    	return 1;
	}
	return 0;
}

public OnFilterScriptInit() {
	WeaponMenu[0] = CreateMenu("Weapons", 1, 470.0, 180.0, 120.0, 0.0);
	WeaponMenu[1] = CreateMenu("Slot 0", 1, 470.0, 180.0, 120.0, 0.0);
	WeaponMenu[2] = CreateMenu("Slot 1", 1, 470.0, 180.0, 120.0, 0.0);
	WeaponMenu[3] = CreateMenu("Slot 2", 1, 470.0, 180.0, 120.0, 0.0);
	WeaponMenu[4] = CreateMenu("Slot 3", 1, 470.0, 180.0, 120.0, 0.0);
	WeaponMenu[5] = CreateMenu("Slot 4", 1, 470.0, 180.0, 120.0, 0.0);
	WeaponMenu[6] = CreateMenu("Slot 5", 1, 470.0, 180.0, 120.0, 0.0);
	WeaponMenu[7] = CreateMenu("Slot 6", 1, 470.0, 180.0, 120.0, 0.0);
	WeaponMenu[8] = CreateMenu("Slot 7", 1, 470.0, 180.0, 120.0, 0.0);
	WeaponMenu[9] = CreateMenu("Slot 8", 1, 470.0, 180.0, 120.0, 0.0);
	WeaponMenu[10] = CreateMenu("Slot 9", 1, 470.0, 180.0, 120.0, 0.0);
	WeaponMenu[11] = CreateMenu("Slot 10", 1, 470.0, 180.0, 120.0, 0.0);
	WeaponMenu[12] = CreateMenu("Slot 11", 1, 470.0, 180.0, 120.0, 0.0);

    AddMenuItem(WeaponMenu[0], 0, "Slot 0");
	AddMenuItem(WeaponMenu[0], 0, "Slot 1");
	AddMenuItem(WeaponMenu[0], 0, "Slot 2");
	AddMenuItem(WeaponMenu[0], 0, "Slot 3");
	AddMenuItem(WeaponMenu[0], 0, "Slot 4");
	AddMenuItem(WeaponMenu[0], 0, "Slot 5");
	AddMenuItem(WeaponMenu[0], 0, "Slot 6");
	AddMenuItem(WeaponMenu[0], 0, "Slot 7");
	AddMenuItem(WeaponMenu[0], 0, "Slot 8");
	AddMenuItem(WeaponMenu[0], 0, "Slot 9");
	AddMenuItem(WeaponMenu[0], 0, "Slot 10");
	AddMenuItem(WeaponMenu[0], 0, "Slot 11");

	AddMenuItem(WeaponMenu[1], 0, "Brass Knuckles");
	AddMenuItem(WeaponMenu[1], 0, "Back");

	AddMenuItem(WeaponMenu[2], 0, "Knife");
	AddMenuItem(WeaponMenu[2], 0, "Golf Club");
	AddMenuItem(WeaponMenu[2], 0, "Shovel");
	AddMenuItem(WeaponMenu[2], 0, "Pool Cue");
	AddMenuItem(WeaponMenu[2], 0, "Nightstick");
	AddMenuItem(WeaponMenu[2], 0, "Baseball Bat");
	AddMenuItem(WeaponMenu[2], 0, "Katana");
	AddMenuItem(WeaponMenu[2], 0, "Chainsaw");
	AddMenuItem(WeaponMenu[2], 0, "Back");

	AddMenuItem(WeaponMenu[3], 0, "Colt 45");
	AddMenuItem(WeaponMenu[3], 0, "Silenced 9mm");
	AddMenuItem(WeaponMenu[3], 0, "Desert Eagle");
	AddMenuItem(WeaponMenu[3], 0, "Back");

	AddMenuItem(WeaponMenu[4], 0, "Shotgun");
	AddMenuItem(WeaponMenu[4], 0, "Sawnoff Shotgun");
	AddMenuItem(WeaponMenu[4], 0, "Combat Shotgun");
	AddMenuItem(WeaponMenu[4], 0, "Back");

    AddMenuItem(WeaponMenu[5], 0, "Tec-9");
	AddMenuItem(WeaponMenu[5], 0, "Uzi");
	AddMenuItem(WeaponMenu[5], 0, "MP5");
	AddMenuItem(WeaponMenu[5], 0, "Back");

	AddMenuItem(WeaponMenu[6], 0, "AK-47");
	AddMenuItem(WeaponMenu[6], 0, "M4");
	AddMenuItem(WeaponMenu[6], 0, "Back");

	AddMenuItem(WeaponMenu[7], 0, "Country Rifle");
	AddMenuItem(WeaponMenu[7], 0, "Sniper Rifle");
	AddMenuItem(WeaponMenu[7], 0, "Back");

	AddMenuItem(WeaponMenu[8], 0, "Flamethrower");
	AddMenuItem(WeaponMenu[8], 0, "Rocket Launcher");
	AddMenuItem(WeaponMenu[8], 0, "Heatseek Rocket Launcher");
	AddMenuItem(WeaponMenu[8], 0, "Minigun");
	AddMenuItem(WeaponMenu[8], 0, "Back");

	AddMenuItem(WeaponMenu[9], 0, "Tear Gus");
	AddMenuItem(WeaponMenu[9], 0, "Molotov Cocktail");
	AddMenuItem(WeaponMenu[9], 0, "Grenade");
	AddMenuItem(WeaponMenu[9], 0, "Satchel Charge");
	AddMenuItem(WeaponMenu[9], 0, "Back");

	AddMenuItem(WeaponMenu[10], 0, "Fire Extinguisher");
	AddMenuItem(WeaponMenu[10], 0, "Spraycan");
	AddMenuItem(WeaponMenu[10], 0, "Camera");
	AddMenuItem(WeaponMenu[10], 0, "Back");

	AddMenuItem(WeaponMenu[11], 0, "Dildo");
	AddMenuItem(WeaponMenu[11], 0, "Double Dildo");
	AddMenuItem(WeaponMenu[11], 0, "Vibrator");
	AddMenuItem(WeaponMenu[11], 0, "Silver Vibrator");
	AddMenuItem(WeaponMenu[11], 0, "Flower");
	AddMenuItem(WeaponMenu[11], 0, "Cane");
	AddMenuItem(WeaponMenu[11], 0, "Back");

	AddMenuItem(WeaponMenu[12], 0, "Parachute");
	AddMenuItem(WeaponMenu[12], 0, "Back");

	return 1;
}

public OnFilterScriptExit() {
	for(new i=0; i<sizeof(WeaponMenu); i++) {
		DestroyMenu(WeaponMenu[i]);
	}
	return 1;
}

public OnPlayerSelectedMenuRow(playerid, row) {
    if(GetPlayerMenu(playerid) == WeaponMenu[0]) {
        switch(row) {
            case 0: HideMenuForPlayer(WeaponMenu[0], playerid), ShowMenuForPlayer(WeaponMenu[1], playerid);
            case 1: HideMenuForPlayer(WeaponMenu[0], playerid), ShowMenuForPlayer(WeaponMenu[2], playerid);
            case 2: HideMenuForPlayer(WeaponMenu[0], playerid), ShowMenuForPlayer(WeaponMenu[3], playerid);
            case 3: HideMenuForPlayer(WeaponMenu[0], playerid), ShowMenuForPlayer(WeaponMenu[4], playerid);
            case 4: HideMenuForPlayer(WeaponMenu[0], playerid), ShowMenuForPlayer(WeaponMenu[5], playerid);
            case 5: HideMenuForPlayer(WeaponMenu[0], playerid), ShowMenuForPlayer(WeaponMenu[6], playerid);
            case 6: HideMenuForPlayer(WeaponMenu[0], playerid), ShowMenuForPlayer(WeaponMenu[7], playerid);
            case 7: HideMenuForPlayer(WeaponMenu[0], playerid), ShowMenuForPlayer(WeaponMenu[8], playerid);
            case 8: HideMenuForPlayer(WeaponMenu[0], playerid), ShowMenuForPlayer(WeaponMenu[9], playerid);
            case 9: HideMenuForPlayer(WeaponMenu[0], playerid), ShowMenuForPlayer(WeaponMenu[10], playerid);
            case 10: HideMenuForPlayer(WeaponMenu[0], playerid), ShowMenuForPlayer(WeaponMenu[11], playerid);
            case 11: HideMenuForPlayer(WeaponMenu[0], playerid), ShowMenuForPlayer(WeaponMenu[12], playerid);
        }
    } else if(GetPlayerMenu(playerid) == WeaponMenu[1]) {
        switch(row) {
            case 0: GivePlayerWeapon(playerid, WEAPON_BRASSKNUCKLE, 1); // Brass Knuckles
			case 1: HideMenuForPlayer(WeaponMenu[1], playerid), ShowMenuForPlayer(WeaponMenu[0], playerid);
        }
    } else if(GetPlayerMenu(playerid) == WeaponMenu[2]) {
        switch(row) {
            case 0: GivePlayerWeapon(playerid, WEAPON_KNIFE, 1); // Knife
		    case 1: GivePlayerWeapon(playerid, WEAPON_GOLFCLUB, 1); // Golf Club
		    case 2: GivePlayerWeapon(playerid, WEAPON_SHOVEL, 1); // Shovel
      		case 3: GivePlayerWeapon(playerid, WEAPON_POOLSTICK, 1); // Pool Cure
      		case 4: GivePlayerWeapon(playerid, WEAPON_NITESTICK, 1); // Nightstick
      		case 5: GivePlayerWeapon(playerid, WEAPON_BAT, 1); // Baseball Bat
      		case 6: GivePlayerWeapon(playerid, WEAPON_KATANA, 1); // Katana
   			case 7: GivePlayerWeapon(playerid, WEAPON_CHAINSAW, 1); // Chainsaw
			case 8: HideMenuForPlayer(WeaponMenu[2], playerid), ShowMenuForPlayer(WeaponMenu[0], playerid);
        }
    } else if(GetPlayerMenu(playerid) == WeaponMenu[3]) {
        switch(row) {
            case 0: GivePlayerWeapon(playerid, WEAPON_COLT45, 999); // Pistol 9mm
	        case 1: GivePlayerWeapon(playerid, WEAPON_SILENCED, 999); // Silenced Pistol
	        case 2: GivePlayerWeapon(playerid, WEAPON_DEAGLE, 999); // Desert Eagle
			case 3: HideMenuForPlayer(WeaponMenu[3], playerid), ShowMenuForPlayer(WeaponMenu[0], playerid);
        }
    } else if(GetPlayerMenu(playerid) == WeaponMenu[4]) {
        switch(row) {
            case 0: GivePlayerWeapon(playerid, WEAPON_SHOTGUN, 999); // Shotgun
	        case 1: GivePlayerWeapon(playerid, WEAPON_SAWEDOFF, 999); // Sawn-off Shotgun
	        case 2: GivePlayerWeapon(playerid, WEAPON_SHOTGSPA, 999); // Combat Shotgun
			case 3: HideMenuForPlayer(WeaponMenu[4], playerid), ShowMenuForPlayer(WeaponMenu[0], playerid);
        }
    } else if(GetPlayerMenu(playerid) == WeaponMenu[5]) {
        switch(row) {
            case 0: GivePlayerWeapon(playerid, WEAPON_TEC9, 999); // Tec-9
	        case 1: GivePlayerWeapon(playerid, WEAPON_UZI, 999); // Micro-Uzi
	        case 2: GivePlayerWeapon(playerid, WEAPON_MP5, 999); // SMG
			case 3: HideMenuForPlayer(WeaponMenu[5], playerid), ShowMenuForPlayer(WeaponMenu[0], playerid);
        }
    } else if(GetPlayerMenu(playerid) == WeaponMenu[6]) {
        switch(row) {
            case 0: GivePlayerWeapon(playerid, WEAPON_AK47, 999); // AK-47
	        case 1: GivePlayerWeapon(playerid, WEAPON_M4, 999); // M4
			case 2: HideMenuForPlayer(WeaponMenu[6], playerid), ShowMenuForPlayer(WeaponMenu[0], playerid);
        }
    } else if(GetPlayerMenu(playerid) == WeaponMenu[7]) {
        switch(row) {
            case 0: GivePlayerWeapon(playerid, WEAPON_RIFLE, 99); // Rifle
	        case 1: GivePlayerWeapon(playerid, WEAPON_SNIPER, 99); // Sniper
			case 2: HideMenuForPlayer(WeaponMenu[7], playerid), ShowMenuForPlayer(WeaponMenu[0], playerid);
        }
    } else if(GetPlayerMenu(playerid) == WeaponMenu[8]) {
        switch(row) {
            case 0: GivePlayerWeapon(playerid, WEAPON_FLAMETHROWER, 999); // Flamethrower
	        case 1: GivePlayerWeapon(playerid, WEAPON_ROCKETLAUNCHER, 999); // Rocket Launcher
	        case 2: GivePlayerWeapon(playerid, WEAPON_HEATSEEKER, 999); // Heat-Seeking
	        case 3: GivePlayerWeapon(playerid, WEAPON_MINIGUN, 999); // Minigun
			case 4: HideMenuForPlayer(WeaponMenu[8], playerid), ShowMenuForPlayer(WeaponMenu[0], playerid);
        }
    } else if(GetPlayerMenu(playerid) == WeaponMenu[9]) {
        switch(row) {
            case 0: GivePlayerWeapon(playerid, WEAPON_TEARGAS, 99); // Tear Gas
	        case 1: GivePlayerWeapon(playerid, WEAPON_MOLTOV, 99); // Molotov Cocktail
	        case 2: GivePlayerWeapon(playerid, WEAPON_GRENADE, 99); // Frag Granade
	        case 3: GivePlayerWeapon(playerid, WEAPON_SATCHEL, 99); // Satchel Charges
			case 4: HideMenuForPlayer(WeaponMenu[9], playerid), ShowMenuForPlayer(WeaponMenu[0], playerid);
        }
    } else if(GetPlayerMenu(playerid) == WeaponMenu[10]) {
        switch(row) {
            case 0: GivePlayerWeapon(playerid, WEAPON_FIREEXTINGUISHER, 9999); // Fire Extinguisher
	        case 1: GivePlayerWeapon(playerid, WEAPON_SPRAYCAN, 99); // Spray Can
	        case 2: GivePlayerWeapon(playerid, WEAPON_CAMERA, 99); // Camera
			case 3: HideMenuForPlayer(WeaponMenu[10], playerid), ShowMenuForPlayer(WeaponMenu[0], playerid);
        }
    } else if(GetPlayerMenu(playerid) == WeaponMenu[11]) {
        switch(row) {
            case 0: GivePlayerWeapon(playerid, WEAPON_DILDO, 1); // Dildo
            case 1: GivePlayerWeapon(playerid, WEAPON_DILDO2, 1); // Double Dildo
            case 2: GivePlayerWeapon(playerid, WEAPON_VIBRATOR, 1); // Vibrator
            case 3: GivePlayerWeapon(playerid, WEAPON_VIBRATOR2, 1); // Silver Vibrator
	        case 4: GivePlayerWeapon(playerid, WEAPON_FLOWER, 1); // Flowers
	        case 5: GivePlayerWeapon(playerid, WEAPON_CANE, 1); // Cane
			case 6: HideMenuForPlayer(WeaponMenu[11], playerid), ShowMenuForPlayer(WeaponMenu[0], playerid);
        }
    } else if(GetPlayerMenu(playerid) == WeaponMenu[12]) {
        switch(row) {
            case 0: GivePlayerWeapon(playerid, WEAPON_PARACHUTE, 1); // Parachute(って武器じゃなくね？)
			case 1: HideMenuForPlayer(WeaponMenu[12], playerid), ShowMenuForPlayer(WeaponMenu[0], playerid);
        }
    }
    return 1;
}

public OnPlayerExitedMenu(playerid) {
    // TogglePlayerControllable(playerid, 1);
    
	return 1;
}

// -----------------------------------------------------------------------------

stock strtok(const string[], &index)
{
	new length = strlen(string);
	while ((index < length) && (string[index] <= ' '))
	{
		index++;
	}

	new offset = index;
	new result[20];
	while ((index < length) && (string[index] > ' ') && ((index - offset) < (sizeof(result) - 1)))
	{
		result[index - offset] = string[index];
		index++;
	}
	result[index - offset] = EOS;
	return result;
}
